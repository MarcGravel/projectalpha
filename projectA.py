def validate(num1, num2, oper):    
    error = ""
    oper_list = ["+", "-", "*", "/"]
    if num1.isnumeric():
        num1 = int(num1)
    else:
        error = num1 + " for first integer"
    if num2.isnumeric(): 
        num2 = int(num2)
    elif error == "": 
        error = num2 + " for second integer"
    
    if oper not in oper_list and error == "":
        error = oper + " for operand"
    
    if oper == "/" and num2 == 0 and error == "":
        error = "denominator can not be 0"

    if error == "":
        return do_math(num1, num2, oper)
    else:  
        print("Invalid input: " + error)
        return restart()

def do_math(num1, num2, oper):
    if oper == '+':
        ttl = num1 + num2
    elif oper == '-':
        ttl = num1 - num2
    elif oper == '*':
        ttl = num1 * num2
    else:
        ttl = num1 / num2
    print("Answer: " + str(ttl))
    return restart()

def restart():
    user_input = input("Enter two integers and an operand (+ - * /) separated by spaces: ")
    num1, num2, oper = user_input.split()
    validate(num1, num2, oper)

user_input = input("Enter two numbers and an operand (+ - * /) separated by spaces: ")
num1, num2, oper = user_input.split()
validate(num1, num2, oper)