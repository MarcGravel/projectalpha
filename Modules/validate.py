import Modules.do_math as do_math 

def validate(first_char, second_char, operand):    
    error = ""
    oper_list = ["+", "-", "*", "/"]
    if first_char.isnumeric():
        int1 = int(first_char)
    else:
        error = first_char + " for first integer"
    if second_char.isnumeric(): 
        int2 = int(second_char)
    elif error == "": 
        error = second_char + " for second integer"
    
    if operand not in oper_list and error == "":
        error = operand + " for operand"
    
    if operand == "/" and int2 == 0 and error == "":
        error = "denominator can not be 0"

    return error