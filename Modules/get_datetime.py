import datetime
def get_date_time():
    date_time_str = datetime.datetime.now().strftime("%I:%M%p on %B %d, %Y")
    return "Time and date of calculation: " + date_time_str