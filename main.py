from Modules import validate, do_math, get_datetime

def restart():
    user_input = input("Enter two integers and an operand (+ - * /) separated by spaces: ")
    first_char, second_char, operand = user_input.split()
    error  =  validate.validate(first_char, second_char, operand)
    if error == "":
        do_math.do_math(int(first_char), int(second_char), operand)
        print(get_datetime.get_date_time())
        restart()
    else:  
        print("Invalid input: " + error)
        restart()
restart()
